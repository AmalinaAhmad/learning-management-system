<x-app-layout>
    <div class="h-screen  bg-black bg-image-element flex flex-row">
        <div class="w-56 bg-blue-nav rounded-r-lg border-1 shadow-xl" id="box1">
            <div class="flex flex-row justify-end">
                <button class="" id="hide">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="" viewBox="0 0 24 24" stroke="#FFFFFF">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    </svg>
                </button>
            </div>
            <div class="flex flex-col ml-3 text-sm font-montserrat text-white">
                <a href=""> Dashboard</a>
                <a class="mt-2" href=""> Class</a>
                <a class="mt-2" href=""> Game</a>
            </div>
        </div>
        <div class="bg-transparent rounded-r-lg border-1 shadow-xl" id="box2" style="display: none;">
            <div class="flex flex-row justify-end">
                <button class="" id="show">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="" viewBox="0 0 24 24" stroke="#FFFFFF">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    </svg>
                </button>
            </div>
        </div>
        <div class="py-16">
            <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                    <x-jet-welcome />
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("#hide").click(function() {
                console.log("click hide")
                $("#box1").hide();
                $("#box2").show();
            });
            $("#show").click(function() {
                $("#box1").show();
                $("#box2").hide();
            });
        });
    </script>
</x-app-layout>
{{-- <x-app-layout>
        <x-slot name="header">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Dashboard') }}
</h2>
</x-slot>
<div class="py-16">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            <x-jet-welcome />
        </div>
    </div>
</div>
</x-app-layout> --}}
