<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                {{-- <x-jet-label for="email" value="{{ __('Email') }}" /> --}}
                <x-jet-input id="email" class="block mt-1 w-full font-montserrat text-blue-tail placeholder-blue-500 " placeholder="Email" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                {{-- <x-jet-label for="password" value="{{ __('Password') }}" /> --}}
                <x-jet-input id="password" class="block mt-1 w-full font-montserrat text-blue-tail placeholder-blue-500" type="password" name="password" placeholder="Password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-jet-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-white font-montserrat">{{ __('Remember me') }}</span>
                </label>
            </div>

            {{-- <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-jet-button class="ml-4">
                    {{ __('Login') }}
                </x-jet-button>
            </div> --}}

            <div class="grid grid-flow-row auto-rows-max mt-6">

                <x-jet-button >
                    {{ __('Login') }}
                </x-jet-button>


                @if (Route::has('password.request'))
                <a class="underline ml-1 text-sm font-montserrat text-blue-tail text-center mt-4" href="{{ route('password.request') }}">
                    {{ __('Forgot your password?') }}
                </a>
             @endif

            


            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
