const defaultTheme = require('tailwindcss/defaultTheme');
module.exports = {
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],
    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
                'montserrat': ['Montserrat' ,'sans-serif'],
                // 'roboto' : ['Roboto'],
                // 'serif': ['ui-serif', 'Georgia', ...],
                'press-start': ['"Press Start 2P"', 'cursive']
            },
            backgroundImage: theme => ({
                'image-element': "url('/img/Background_Transparent.png')",
                'image-landing': "url('/img/landing.png')",

               }),

            textColor: theme => theme('colors'),
               textColor: {
                 'blue-nav': '#D1E0FB',
                 'blue-tail': '#4579F5',
                 'blue-regis': '#377DFF',

           
               },

               backgroundColor: theme => ({
                ...theme('colors'),
                'blue-nav': '#377DFF',

               }),
               
               borderColor: theme => ({
                ...theme('colors'),
                DEFAULT: theme('colors.gray.300', 'currentColor'),
                'primary': '#3490dc',
                'secondary': '#ffed4a',
                'danger': '#e3342f',
                'blue-nav': '#D1E0FB',

               }),
               placeholderColor: theme => theme('colors'),
               placeholderColor: {
                'blue-default': '#377DFF',
         
               }
         
        },
    },
    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },
    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
